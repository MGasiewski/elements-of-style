import nltk
import re


 # wh_paranthetical_chunk = chunk.regexp.ChunkRule("<W.*><NN.?|JJ.?|PRP>*<VB.?>(<DT>?<JJ.?|NN.?>)*(<IN><DT|PRP$>?<JJ.?>*<NN.?>*)?", "wh-paranthetical")
 #    noun_paranthetical_chunk = chunk.regexp.ChunkRule("<PRP$><NN|PRP>", "noun phrase")
 #
 #    if paranthetical_chunk == "wh":
 #        chunk_parser = RegexpChunkParser([wh_paranthetical_chunk], chunk_label="wh- paranthetical")
 #    elif paranthetical_chunk == "noun":
 #        chunk_parser = RegexpChunkParser([noun_paranthetical_chunk], chunk_label="noun phrase")



# def process_sentence(text):
#     return nltk.pos_tag(nltk.tokenize.word_tokenize(text))
#
# adj_series_chunk = "<JJ.?><,>?(<JJ.?><,>?)+<CC>?<JJ.?>"
# noun_series_chunk = nltk.chunk.regexp.ChunkRule("<DT>?<JJ.?>*<NN.?|PRP.?><,>?(<DT>?<JJ.?>*<NN.?|PRP.?><,>?)+<CC>?<DT>?<JJ.?>*<NN.?|PRP.?>", "noun series")
# verb_series_chunk = nltk.chunk.regexp.ChunkRule("<VB.?><.*>*?<,>?<.*>*?<VB.?><.*>*?<,>?<CC><.*>*?<VB.?>?", "verb series")
# chunk_parser = nltk.RegexpChunkParser([verb_series_chunk], chunk_label="verb_series")
#
# tagged_sent = (process_sentence("I opened the door, ran to the car, beat his dog, murdered his boss, and started driving"))
# tree = (chunk_parser.parse(tagged_sent))
#
# for i in tree:
#     if type(i) is nltk.tree.Tree:
#         series = [j[1][0:2] for j in i]
#         series_phrase_words = [j[0] for j in i]
#
# series_phrase = ""
#
# for i in series_phrase_words:
#     if i == ',':
#         series_phrase = series_phrase[:len(series_phrase) - 1]
#     series_phrase += i + " "
#
# number_of_commas = series.count(',')
# number_of_verbs = series.count('VB')
# print(number_of_commas, number_of_verbs, series_phrase)

grammar = r"""
    NP : {<DT|PRP$>?<JJ.?>*<NN.?|PRP>+}
    WP : {<W.*><NN.?|JJ.?|PRP>*<VB.?>(<DT>?<JJ.?|NN.?>)*(<IN><DT|PRP$>?<JJ.?>*<NN.?>*)?}
  """
#NPE : {<NN.?><,>?<DT>?<NN.?>+<,>?}

cp = nltk.RegexpParser(grammar)

sentence = nltk.pos_tag(nltk.word_tokenize("Larry the woodworker, who is wonderful, at his job"))
print(cp.parse(sentence))
print(sentence)
