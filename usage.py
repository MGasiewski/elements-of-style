from nltk import tokenize, pos_tag, chunk, RegexpChunkParser, tree, RegexpParser
import re


def rule_one(sent_tags):
    for i in range(len(sent_tags)):
        if re.match("^\w+?s$", sent_tags[i][0]) and (sent_tags[i][1] == 'NNP' or sent_tags[i][1] == 'NN') \
                and sent_tags[i + 1][0] != "'s":
            print(str(sent_tags[i][0]), "fails rule one")


def rule_two(sent_tags, pos_series_chunk="noun"):
    """series of 3 or more items"""
    adj_series_chunk = chunk.regexp.ChunkRule("<DT>?<JJ.?><,>(<DT>?<JJ.?><,>)+<CC>?<DT>?<JJ.?>", "adjective series")
    noun_series_chunk = chunk.regexp.ChunkRule(
        "<DT>?<JJ.?>*<NN.?|PRP.?><,>?(<DT>?<JJ.?>*<NN.?|PRP.?><,>?)+<CC>?<DT>?<JJ.?>*<NN.?|PRP.?>", "noun series")
    verb_series_chunk = chunk.regexp.ChunkRule("<VB.?><.*>*?<,>?(<.*>*?<VB.?><.*>*?<,>?)+<CC>?<.*>*?<VB.?>?",
                                               "verb series")
    if pos_series_chunk == "verb":
        chunk_parser = RegexpChunkParser([verb_series_chunk], chunk_label="verb_series")
    elif pos_series_chunk == "noun":
        chunk_parser = RegexpChunkParser([noun_series_chunk], chunk_label="noun_series")
    elif pos_series_chunk == "adjective":
        chunk_parser = RegexpChunkParser([adj_series_chunk], chunk_label="adjective_series")

    word_tree = (chunk_parser.parse(sent_tags))

    series = []

    for i in word_tree:
        if type(i) is tree.Tree:
            series = [j[1][0:2] for j in i]
            series_phrase_words = [j[0] for j in i]

    if series == []:
        return

    series_phrase = ""

    for i in series_phrase_words:
        if i == ',':
            series_phrase = series_phrase[:len(series_phrase) - 1]
        series_phrase += i + " "

    number_of_commas = series.count(',')
    if pos_series_chunk == 'verb':
        number_of_items = series.count('VB')
    elif pos_series_chunk == 'noun':
        number_of_items = series.count('NN')
    elif pos_series_chunk == 'adjective':
        number_of_items = series.count('JJ')

    if number_of_commas != number_of_items - 1:
        print("Incorrect number of commas for series.", series_phrase)
    else:
        return


def rule_three(sent_tags):
    """enclose paranthetical expressions with commas"""

    grammar = r"""
    NP : {<DT|PRP\$>?<JJ.?>*<NN.?|PRP>+}
    PREP : {(<IN|TO><NP>)+}
    VP : {<MD>?<VB.?|RB>+}
    COMMA : {<,>}
    PUNC : {<.|;|:>}
    WP : {<W.*><NP>*<VP><NP>*<PREP>?}
    DATE: {<NP><CD><COMMA>?<CD><COMMA|PUNC>?}
    """

    chunk_parser = RegexpParser(grammar)
    word_tree = (chunk_parser.parse(sent_tags))
    for i in word_tree:
        if type(i) is tree.Tree:
            if i.label() == "DATE":
               if i[2][0][0] != ',' and len(i) < 5:
                   print("Date is not set off by punctuation. "
                         "The year should have a comma on the left and another punctuation mark on the right")
            elif i.label() == "WP":
                if word_tree[word_tree.index(i) -1].label() != 'COMMA' or len(word_tree) < word_tree.index(i)+2 or \
                        (word_tree[word_tree.index(i)+1].label() != 'COMMA' or word_tree[word_tree.index(i)+1].label() != 'PUNC'):
                    print("Paranthetical expression is mission one or both of its commas")
            elif i.label() == "NP":
                if len(word_tree) > word_tree.index(i)+2 and word_tree[word_tree.index(i)+1].label() == "NP":
                    print("Consecutive noun phrases, use a commas to enclose the second one")

def rule_four(sent_tags):
    """place a comma before a conjunction introducing an independent clause"""
    independent_clause_chunk = chunk.regexp.ChunkRule("<CC><.*>*?<NN.?|PRP><.*>*?<VB.?><.*>*?",
                                                      "conjunction with coordinate clause")
    chunk_parser = RegexpChunkParser([independent_clause_chunk], chunk_label="independent clause chunk")
    sentence_tree = (chunk_parser.parse(sent_tags))
    for i in range(len(sentence_tree)):
        if type(sentence_tree[i]) is tree.Tree and sentence_tree[i - 1] != ',':
            print("There is no comma before the conjunction: " + sentence_tree[i][0][0])
    return


def rule_five(sent_tags):
    """do not join independent clauses with a comma"""
    tags = [j[1][:2] for j in sent_tags]
    if ',' not in tags:
        return
    first = tags[:tags.index(",")]
    second = tags[tags.index(",") + 1:]
    if 'VB' not in first or 'NN' not in first:
        return
    if 'VB' not in second or 'NN' not in second:
        return
    if first[0] == 'IN' or first[0] == 'WR' or second[0] == "CC":
        return
    else:
        print("Sentence needs a conjunction after the comma.")


def rule_six(sent_tags):
    """do not break sentences into two"""
    tags = [j[1][:2] for j in sent_tags]
    if 'VB' not in tags or 'NN' not in tags:
        print("This is not a complete sentence.")
    else:
        return


def rule_seven(sent_tags):
    """a participal phrase at the beginning of a sentence must refer to the grammatical subject"""
    grammar = r"""
    NP : {<DT|PRP\$>?<JJ.?>*<NN.?|PRP>+}
    VP : {<MD>?<VB.?|RB>+}
    COMMA : {<,>}
    PUNC : {<.|;|:>}
    P_PHRASE: {^(<IN|TO><VP|NP>)+<COMMA>}
    """
    chunk_parser = RegexpParser(grammar)
    word_tree = (chunk_parser.parse(sent_tags))
    if type(word_tree[0]) is tree.Tree:
        if word_tree[0].label() == "P_PHRASE":
            print("Sentence has beginning participial phrase. Please make sure it refers to the subject")
