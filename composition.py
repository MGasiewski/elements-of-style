from nltk import tokenize, classify, pos_tag, RegexpParser, tree
import re

tag_and_tokenize = lambda sentence: pos_tag(tokenize.word_tokenize(sentence))

def rule_eight(para):
    """Make the paragraph the unit of composition: one paragraph to each topic"""
    pass

def rule_nine(para):
    """As a rule, begin each paragraph with a topic sentence; end it in conformity with the beginning"""
    pass

def rule_ten(sentence):
    """Do not use passive voice"""
    for i in range(len(sentence) - 1):
        if (sentence[i][1] == 'VBZ' or sentence[i][1] == 'VBD') and (sentence[i+1][1] == 'VBG' or sentence[i+1][1] == 'VBN'):
            return "\"" + sentence[i][0] + " " + sentence[i+1][0] + "\" " + "is passive voice. Avoid using passive voice."

def rule_eleven(sentence):
    """Put statements in positive form"""
    word_tokens = tokenize.word_tokenize(sentence)
    for word in word_tokens:
        if re.match("[Nn][Oo][Tt]", word):
            return "Sentence contains idea(s) expressed in negative form. Try removing the word 'not' and expressing the sentence differently"


def rule_twelve(sentence):
    """Use definite, specific, concrete language"""
    imprecise_words = ["some", "few", "many", "nearly", "almost", "very", "little", "lots", "a lot", "kind of",
                       "sort of"]
    for word in imprecise_words:
        if word in sentence or word.capitalize() in sentence:
            print(word.capitalize() + " is imprecise. Make your sentence more concrete by using a definite term")

def rule_thirteen(sentence):
    """This rule looks for what Strunk calls "needless words" and asks the user to remove them"""
    needless_words_and_phrases = ["as to", "there is", "who is", "which", "who", "which", "the fact that", "this",
                                  "basically", "being that", "each and every", "on account of"]
    for phrase in needless_words_and_phrases:
        if phrase in sentence:
            return "Try rewriting '" + phrase + ".' It is a weak construction."

def rule_fourteen(paragraph):
    """This rule looks for "a loose succession of sentences" and marks them for removal
    this is achieved by checking for successive multi-clause sentences
    each clause having a verb phrase and a comma"""
    para_tags = tag_and_tokenize(paragraph)

    grammar = r"""
    VP : {<MD|TO>?<VB.?|RB>+}
    COMMA : {<,>}
    PERIOD : {<.>}"""

    chunk_parser = RegexpParser(grammar)
    word_tree = (chunk_parser.parse(para_tags))

    vp = 0
    comma = 0
    two_clause_sent = 0
    for i in word_tree:
        if type(i) is tree.Tree:
            print(i.label())
            if i.label() == "VP":
                vp += 1
            if i.label() == "COMMA":
                comma += 1
            if i.label() == "PERIOD":
                if vp > 1 and comma > 0:
                    two_clause_sent += 1
                else:
                    two_clause_sent = 0
                vp = 0
                comma = 0
        if two_clause_sent > 1:
            return "Succession of two-clause or more sentences detected. Break up sentences into" \
                   " smaller sentences."
    return

def rule_fifteen(sentence):
    """express coordinate ideas in similar form"""
    grammar = r"""
    ADJ_SERIES : {<DT>?<JJ.?><,>(<DT>?<JJ.?><,>)+<CC>?<DT>?<JJ.?>}
    NOUN_SERIES : {<DT>?<JJ.?>*<NN.?|PRP.?><,>?(<DT>?<JJ.?>*<NN.?|PRP.?><,>?)+<CC>?<DT>?<JJ.?>*<NN.?|PRP.?>}
    VERB_SERIES : {<VB.?><.*>*?<,>?(<.*>*?<VB.?><.*>*?<,>?)+<CC>?<.*>*?<VB.?>?}"""

    sent_tags = tag_and_tokenize(sentence)
    chunk_parser = RegexpParser(grammar)
    word_tree = (chunk_parser.parse(sent_tags))

    for t in word_tree:
        if type(t) is tree.Tree:
            if t.label() == "ADJ_SERIES":
                form = []
                for item in t:
                    if item[2] == ",":
                        pass
                    else:
                        form = form + item[2]
            elif t.label() == "NOUN_SERIES":
                pass
            elif t.label() == "VERB_SERIES":
                pass


def rule_sixteen(sentence):
    """Keep related words together: don't separate the subject, verb and object"""
    grammar = r"""
    NP : {<DT|PRP\$>?<JJ.?>*<NN.?|PRP>+}
    VP : {<MD|TO>?<VB.?|RB>+}
    COMMA_PHRASE : {<,><.*>*?<,>}
    PERIOD : {<.>}"""

    sent_tags = tag_and_tokenize(sentence)
    chunk_parser = RegexpParser(grammar)
    word_tree = (chunk_parser.parse(sent_tags))

    vp = 0
    comma = 0
    for i in word_tree:
        if type(i) is tree.Tree:
            print(i.label())
            if i.label() == "VP":
                vp += 1
            if i.label() == "COMMA_PHRASE":
                comma += 1
            if i.label() == "NP" and vp > 0 and comma > 0:
                return "Keep subject, verb, and object together. Do not separate with comma"
    return


def rule_seventeen(text):
    """use one tense for summaries"""
    text_tags = tag_and_tokenize(text)
    tense = ""
    for word in text_tags:
        if re.match("VB.", word[0]):
            tense = word[0]
        if tense != "" and re.match("VB.", word[0]):
            if word[0] != tense:
                return "Text does not have a consistent tense. If this is a summary" \
                       ", keep to one tense."

def rule_eighteen(sentence):
    """place the emphatic words at the end of the sentence (or the action words)"""
    grammar = r"""
    NP : {<DT|PRP\$>?<JJ.?>*<NN.?|PRP>+}
    PREP : {(<IN|TO><NP>)+}
    VP : {<MD>?<VB.?|RB>+}
    COMMA : {<,>}
    PERIOD : {<.>}
    WP : {<W.*><NP>*<VP><NP>*<PREP>?}
    DATE: {<NP><CD><COMMA>?<CD><COMMA|PUNC>?}
    CLAUSE: {<NP|VP><NP|VP>}
    CONJ: {<CC>}
    """

    sent_tags = tag_and_tokenize(sentence)
    chunk_parser = RegexpParser(grammar)
    word_tree = (chunk_parser.parse(sent_tags))

    token_list = []

    for i in word_tree:
        if type(i) is tree.Tree:
            token_list.append(i.label())

    print(token_list)

    for i in range(len(token_list)):
        if token_list[i] == "CLAUSE":
            for j in range(i, len(token_list)):
                if token_list[j] == "COMMA" and token_list[j+1] != "CONJ":
                    return "Put the clause of the sentence at the end. The sentence is stronger" \
                    " if you end with the main idea"
    return


print(rule_fifteen("The French, the Italian, the Spanish, and the Portuguese."))