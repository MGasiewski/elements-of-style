from nltk import tokenize, pos_tag
import usage, composition

class document(object):

    text = ""
    paragraphs = []
    sentences = []
    tokenized_sentences = []
    tagged_sentences = []

    def __init__(self, text):
        self.text = text
        self.sentences = tokenize.sent_tokenize(text)
        #ttt = tokenize.TextTilingTokenizer()
        self.parasgraphs = text.split("\n\n")
        self.sentences = tokenize.sent_tokenize(text)
        self.tokenized_sentences = [tokenize.word_tokenize(sent) for sent in self.sentences]
        self.tagged_sentences = [pos_tag(sentence) for sentence in self.tokenized_sentences]

    def analyze_usage(self):

        for sentence in self.tagged_sentences:
            print("YOUR SENTENCE:", self.sentences[self.tagged_sentences.index(sentence)])
            usage.rule_one(sentence)
            usage.rule_two(sentence)
            usage.rule_three(sentence)
            usage.rule_four(sentence)
            usage.rule_five(sentence)
            usage.rule_six(sentence)
            usage.rule_seven(sentence)

    def analyze_comp(self):
        pass



doc = document("Hello, do you like my hat? No I do not like your hat. Okay. Goodbye.")
doc.analyze_usage()

